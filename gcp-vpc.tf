# Script to build VPC and Peering over GCI for demos

provider "google" {
 credentials = file("/home/jignrig/credentials/gcp_crendentials.json")
 project     = "adept-shade-286019"
 region      = var.region
}

resource "google_compute_network" "vpc-network-demo" {
  name                       = "vpc-network-demo"
  routing_mode               = "REGIONAL" 
  auto_create_subnetworks    = false
}

resource "google_compute_subnetwork" "vpc-subnet-demo" {
    ip_cidr_range            = "10.30.0.0/24"
    name                     = "vpc-subnet-demo"
    network                  = google_compute_network.vpc-network-demo.id
}

resource "google_compute_router" "vpc-rtr-demo" {
  name    = "vpc-rtr-demo"
  network = google_compute_network.vpc-network-demo.id
  bgp {
    asn               = "16550"
  }
   
}

resource "google_compute_interconnect_attachment" "vpc-vla-demo" {
  name                      = "vpc-vla-demo"
  router                    = google_compute_router.vpc-rtr-demo.id
  type                      = "PARTNER"
  edge_availability_domain  = "AVAILABILITY_DOMAIN_1"
  
   }

  output "pairing_key" {
    value       = google_compute_interconnect_attachment.vpc-vla-demo.pairing_key
 }

